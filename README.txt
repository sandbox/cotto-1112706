This module shows any image that would have been displayed by imagecache as a 
dummy image. If you are working on localhost or a dev server and all the images
are on the production server - this is the module for you.

To install, enable the module and go to admin/build/imagecache/dummyimage and
configure in which cases you want dummy images to be used. You can also adjust
dummy image appearances there.

The module (quite hostilely) takes over the theming function for imagecache 
and can not be overridden from there. 

The module uses the awesome dummy image generator service made by:
Russell Heimlich (@kingkool68)
http://dummyimage.com/
