<?php

/**
 * Implementation of hook_drush_command()
 */

function dummyimage_drush_command() {
  $items = array();

  $items['deploy-lasers'] = array(
      'description' => "",
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      );
  $items['panic-button'] = array(
      'description' => "",
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      );
  $items['unpanic-button'] = array(
      'description' => "",
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      );


  return $items;
}

/**
 * Implementation of drush_hook_COMMAND().
 */

/*disable dummyimage asap*/
function drush_dummyimage_panic_button() {
  variable_set('dummyimages_generate', "1");
  drush_print("panicked :(");
}

function drush_dummyimage_unpanic_button() {
  variable_set('dummyimages_generate', "3");
  drush_print("unpanicked :)");
}


function drush_dummyimage_deploy_lasers() {
  $rid = db_result(db_query('SELECT rid FROM {role} WHERE name = "webmaster"'));
  variable_set('dummyimage_kittens', 1);
  variable_set('dummyimage_stealth', 1);
  variable_set('dummyimage_only_on_date', 1);
  variable_set('dummyimage_role_list', array($rid => "$rid"));
  variable_set('dummyimages_generate', "3");
  $form = $form_state = array();
  dummyimage_stealth_update($form, $form_state);
  drush_print("lasers deployed");
}

